zfmk-dl buildout

    virtualenv --no-setuptools .

    if that fails:

    virtualenv .
    ./bin/pip install -U setuptools

    and then

    ./bin/python bootstrap.py

    if the authenticity of host 'github.com (192.30.252.130)' can't be established:

    add your public SSH key to 

    https://github.com/settings/ssh

    and

    git config --global user.name "Maik Röder"
    git config --global user.email "maikroeder@gmail.com"

    now continue with

    ./bin/buildout -U

    Run the download using a url

    ./bin/zfmk_dl /tmp

Running supervisor

    ./bin/supervisord -c parts/supervisor/supervisord.conf 

Debugging

    ./bin/supervisord -c parts/supervisor/supervisord.conf -n -e debug
