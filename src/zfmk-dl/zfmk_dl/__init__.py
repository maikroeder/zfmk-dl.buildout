# -*- coding: utf-8 -*-
import glob
import os
import sys
import random
from zipfile import ZipFile
from youtube_dl.downloader.http import HttpFD
from youtube_dl.YoutubeDL import YoutubeDL
from youtube_dl.utils import setproctitle
import simplejson
import codecs


class ZDL(YoutubeDL):
    def __init__(self, params=None, auto_init=True):
        """Create a FileDownloader object with the given options."""
        YoutubeDL.__init__(self, params, auto_init)

class ZFD(HttpFD):
    def __init__(self, ydl, params):
        """Create a FileDownloader object with the given options."""
        HttpFD.__init__(self, ydl, params)

def download(filename, info_dict):
    zdl = ZDL({'socket_timeout': 10})
    zfd = ZFD(zdl, {})
    zfd.download(filename, info_dict)

def include_readme(folder_path, md5sum, batch_size):
    readme_file_path = os.path.join(folder_path, 'Readme.txt')
    if os.path.exists(readme_file_path):
        return
    body = u"""BiNHum Media Export

    The name of the Zip file
    """

    body = u"""
        %s_batch_%s.zip 
    """ % (md5sum, batch_size)

    body += u"""
    is composed of

        1. An md5sum to uniquely identify the exported BiNHum query
        2. the batch number of the download when there are multiple batches

    The Zip file contains

        1. all downloaded media files for this batch
        2. a CSV file containing the metadata for the media files
        3. a JSON file with information on the query

    This is an example directory:
        
        ├── 4528907dc40d43bedf9a80e18077b217_batch_0
        │   ├── 1_1.jpg.json
        │   ├── 1_1.jpg.json
        │   ├── 2_1.jpg.json
        │   ├── 3_1.jpg.json
        │   ├── 3_2.jpg.json
        │   ├── Readme.txt
        │   ├── export.csv
        │   └── query.json

    The export.csv file contains the meta information for all lines with media files, 
    and has the following headers:

        scientificName
        fullScientificName
        genus
        country
        locality
        longitude
        latitude
        coordinateErrorDistanceInMeters
        institutioncode
        collectorname
        identifiername
        gatheringyear
        gatheringdate
        collectioncode
        recordbasis 
        type_status
        mm_url
        tripplestoreid
        source_url  

    The query file has the following information:

        {"query_string": "", 
         "solr_url": "http://solr.binhum.net/solr/collection1/select?wt=json&q=*&sort=scientificName%20asc", 
         "md5sum": "8426c046cdfb4a8879a395628ae95626", 
         "from": []}

    The following information is contained here:

        1. a query string of the URL
        2. the Solr query that was used
        3. the md5sum
        4. the URL parts leading up to the query string
  
    The reason a special naming scheme is used for downloaded files are:

        1. It is not always clear what the file name should be, especially when you have URLs like this:

            http://mediastorage.bgbm.org/fsi/server?type=image&profile=jpeg&quality=100&source=Algaterra%2FAlgae%2FUlnaria_ulna__2013-07-07whk02_Alant_i02_movie.zvi"

        2. Avoid file name collisions

        3. Referring back to the CSV file lines is easy

    When two files come from the same line, they share a common prefix, and the second number
    defined the order in which they are found in the database:

        ├── 4528907dc40d43bedf9a80e18077b217_batch_0
        │   ├── 3_1.jpg.json
        │   ├── 3_2.jpg.json

    When the file type has not been recognized, there remains an error file like this:

        ├── 4528907dc40d43bedf9a80e18077b217_batch_0
        │   ├── 3_2.unknown.todo.error.json

    This file can look like that:    

        {
         "url": "http://mediastorage.bgbm.org/fsi/server?type=image&profile=jpeg&quality=100&source=Algaterra%2FAlgae%2FUlnaria_ulna__2013-07-07whk02_Alant_i02_movie.zvi",
         "exception": "<class 'urllib2.HTTPError'>: HTTP Error 404: Not Found",
         "filename": "4528907dc40d43bedf9a80e18077b217/3_2.unknown"
        }

    The original url from which the download was attempted is contained in this JSON file, 
    the exception that was raised while trying to download it, as well as the filename under 
    which it would have been stored. 
    """
    readme_file = codecs.open(readme_file_path, 'w' ,encoding='utf-8')
    readme_file.write(body)
    readme_file.close()  

def make_zip(container, content, md5sum, batch_size):
    zipfile = os.path.join(container, '%s_batch_%s.zip' % (md5sum, batch_size))
    if os.path.exists(zipfile):
        return
    os.chdir(container)
    files_to_zip = glob.glob(content + '/*')
    with ZipFile(zipfile, 'w') as myzip:
        for file_name in files_to_zip:
            myzip.write(file_name)

def main(argv=None):
    setproctitle(u'zfmk-dl')
    if len(sys.argv) != 2:
        print "Please specify the download folder on the command line."
    download_folder = unicode(sys.argv[-1])
    todos = [file_name for file_name in glob.glob(download_folder + u'/*/*/*_*.*.todo.json')]
    if len(todos) == 0:
        return
    todo_choice = random.choice(todos)
    todo_file = open(todo_choice, 'r')
    todo_content = todo_file.read()
    todo_file.close()
    todo = simplejson.loads(todo_content)
    infodict = {'url':unicode(todo['url'])}
    first_path, file_name = os.path.split(todo_choice)
    md5sum, batch_size = first_path.split('_batch_')
    try:
        download(os.path.join(first_path, unicode(todo['filename'])), infodict)
    except:
        todo['exception'] = "%s: %s" % sys.exc_info()[:2]
        error_file_name = todo_choice[:-len('.json')] + '.error.json'
        error_file = open(os.path.join(first_path, error_file_name), 'w')
        error_file.write(simplejson.dumps(todo, indent=True))
        error_file.close()                
    os.unlink(todo_choice)
    if len(todos) == 1:
        include_readme(first_path, md5sum, batch_size)
        container, content = os.path.split(first_path)
        make_zip(container, content, md5sum, batch_size)
