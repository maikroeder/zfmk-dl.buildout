#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import os.path
from setuptools import setup, find_packages
here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()
requires = [
    'requests',
    ]

setup(name='zfmk_dl',
      version='0.0',
      description='Media file downloader',
      long_description=README + '\n\n' + CHANGES,
      url="https://bitbucket.org/maikroeder/zfmk-dl.buildout",
      author='Maik Röder',
      author_email='maikroeder@gmail.com',
      classifiers=[
        "Topic :: Multimedia",
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "License :: Public Domain",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3"
        ],
      keywords='download',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      entry_points={
        'console_scripts': [
          'zfmk_dl = zfmk_dl:main',
         ],
      }
     )
